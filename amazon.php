<?php
  class AmazonResult {
    const COLOR_IMAGES_RE = '/ImageBlockATF.*?var data = (.*?colorImages.*?);/is';
    const IMAGE_GALLERY_RE = '/ImageBlockATF.*?var data = (.*?imageGalleryData.*?);/is';
    const AUDIBLE_RE = '/"audibleData"\s*?:\s*?audibleData,\n?/';
    const DATE_RE = '/((?:jan(?:uary|\s)|feb(?:ruary|\s)|mar(?:ch|\s)|apr(?:il|\s)|may\s|jun(?:e|\s)|jul(?:y|\s)|aug(?:ust|\s)|sep(?:tem|\s)|oct(?:ober|\s)|nov(?:ember|\s)|dec(?:ember|\s)).*$)/i';
    private $document;
    private $xpath;
    private $response;
    public $imgs;
    public $title;
    public $type;
    public $date;
    public static function fromURL($url) {
      return new self($url);
    }
    private function imgToHiRes($v) {
      return $v['hiRes'];
    }
    private function imgToMainUrl($v) {
      return $v['mainUrl'];
    }
    private function parseImgs() {
      $jsonMatch = array();
      preg_match(self::COLOR_IMAGES_RE, $this->response, $jsonMatch);
      if (count($jsonMatch)) {
        $jsonMatch = preg_replace('/\'/', '"', $jsonMatch[1]);
        $imgs = json_decode($jsonMatch, true);
        $imgs = $imgs['colorImages']['initial'];
        $this->imgs = array_map(array($this, 'imgToHiRes'), $imgs);
      } else {
        preg_match(self::IMAGE_GALLERY_RE, $this->response, $jsonMatch);
        $jsonMatch = preg_replace('/\'/', '"', $jsonMatch[1]);
        $jsonMatch = preg_replace(self::AUDIBLE_RE, '', $jsonMatch);
        $imgs = json_decode($jsonMatch, true);
        $imgs = $imgs['imageGalleryData'];
        $this->imgs = array_map(array($this, 'imgToMainUrl'), $imgs);
      }
    }
    private function loadURL($url) {
      $this->response = file_get_contents($url);
      $this->document = new DOMDocument();
      $this->document->loadHTML($this->response);
      $this->xpath = new DOMXpath($this->document);
    }
    private function parseDate() {
      $matches = array();
      preg_match(self::DATE_RE, $this->xpath->query('//h1[@id="title"]/span[position()=3]')[0]->textContent, $matches);
      if (count($matches))
        $this->date = strtotime($matches[1]);
      else $this->date = null;
    }
    public function __construct($url) {
      $this->loadURL($url);
      $this->parseImgs();
      $this->parseDate();
      $this->title = $this->xpath->query('//span[@id="productTitle"]')[0]->textContent;
      $this->type = $this->xpath->query('//h1[@id="title"]/span[position()=2]')[0]->textContent;
    }
  }
?>
